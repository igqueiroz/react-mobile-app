import React, { createContext, useState, useEffect } from 'react';
import { useUser } from '../auth/useUser';
import { useToken } from '../auth/useToken';
import storage from '../api/storage';
export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
const [ user, setUser, setAutoUser ] = useUser();
const [ token, setToken, removeToken, setTokenInternal ] = useToken();

  async function logout() {
    const tokenLogout = await removeToken();
    setAutoUser(tokenLogout)
    setUser(tokenLogout)
    setTokenInternal(tokenLogout)
    return { success: true }
  }

  useEffect(() => {
    // autoLogin if has token storage in AsyncStorage
    // disable autoLogin here
    const getData = async () => {
        const data = await storage.getData('token');
        // request fingertip function before let autoUser works
        if (data) setAutoUser(data);
    }
    getData().catch((e) => console.error(e));
  },[])

  return (
    <AuthContext.Provider value={ { logout, user, token, setToken, setAutoUser } }>
      { children }   
    </AuthContext.Provider>
  )
}