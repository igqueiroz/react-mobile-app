import React, {useContext} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { AuthContext } from '../../store/AuthProvider'
import Constants from 'expo-constants';

const Home = (props) => {
  const { user, logout } = useContext(AuthContext);
  const logoutFunc = async () => {
    let result = await logout();
    return result;
  }

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Estou logado</Text>
      <TouchableOpacity onPress={ logoutFunc } >
        <Text>Logout User:( { user.id } )</Text>
      </TouchableOpacity>
      <Text>{ Constants.manifest.extra.APP_ENV }</Text>
      <Text>{ Constants.manifest.extra.APP_ENV }</Text>
      <Text></Text>
    </View>
  )
}

export default Home