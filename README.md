# React Native App #

This is the React Native App for Android and iOS. Its just a generic app with authentication and login features with JWT.

## Quick Start ###
---

1) ```npm run deploy:local``` Watch Expo Local

1) ```npm run publish:production``` Publish Expo Production App

1) ```npm run publish:homolog``` Publish Expo App Homolog App

1) ```npm run deploy:production``` Build and Deploy Production APK

1) ```npm run deploy:homolog``` Build and Deploy Homolog Env APK

1) ```npm run storybook``` Watch Storybook Debug

### Config ###

Change `` eas.json `` file for local configuration proxy.

### Folder Structure ###
---
```
src
├── components
│   └── Login
│       │── index.js
│       └── index.js
├── assets (arquivos estruturais)
│   │── images
│   │── icons
│   └── fonts
├── config (env, add-ons, etc...)
├── services (APIs Providers)
├── store (Hooks Providers)
│   config
├── main.dev.css
└── main.js
```

# NodeJs Serverless AppService #

This is a Heroku Backend made for React Native App

### Stack ###

* React Native 0.64
* Expo Framework 44
* Storybook
* Jest
* JWT
* [Expo CLI](https://docs.expo.dev/workflow/expo-cli/)
* [Backend API Code](https://bitbucket.org/igqueiroz/mobile-app-native-node)

### App Development Mode APK ###

[APK pre-release v0](https://www.igorqueiroz.com.br/activation/files/a9f16902-d620-4665-a0a6-85f0050e8261-498955c8158d4903af794da66f50edb5.apk)
